﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.SpecFlow
{
    using System.Collections.Generic;
    using System.Linq;
    using TechTalk.SpecFlow;

    /// <summary>
    /// SpecFlow extensions.
    /// </summary>
    public static class SpecFlowExtensions
    {
        /// <summary>
        /// Get list of values from specified row.
        /// </summary>
        /// <param name="table">The table to get the values from.</param>
        /// <param name="rowIndex">Index of the row to get the values from.</param>
        /// <returns>List of values.</returns>
        public static string[] GetRowValues(this Table table, int rowIndex = 0)
        {
            return (from column
                    in table.Rows[rowIndex]
                    select column.Value).ToArray<string>();
        }

        /// <summary>
        /// Get value from scenario context.
        /// </summary>
        /// <typeparam name="T">Type of value.</typeparam>
        /// <param name="scenarioContext">Scenario context.</param>
        /// <param name="key">The Key.</param>
        /// <returns>The value.</returns>
        public static T GetValue<T>(this ScenarioContext scenarioContext, string key)
        {
            try
            {
                return (T)scenarioContext[key];
            }
            catch (KeyNotFoundException)
            {
                return default(T);
            }
        }

        /// <summary>
        /// Set value in scenario context (or add if doesn't exist)
        /// </summary>
        /// <param name="scenarioContext">Scenario context.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void SetValue(this ScenarioContext scenarioContext, string key, object value)
        {
            if (!scenarioContext.ContainsKey(key))
            {
                scenarioContext.Add(key, value);
            }
            else
            {
                scenarioContext[key] = value;
            }
        }
    }
}
