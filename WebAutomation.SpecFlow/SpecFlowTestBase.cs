﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.SpecFlow
{
    using System;
    using System.Linq;
    using System.Reflection;
    
    using TechTalk.SpecFlow;
    using WebAutomation.Core;
    using WebAutomation.Core.WebObjects.WebComponents;

    /// <summary>
    /// Base class for SpecFlow tests.
    /// </summary>
    [Obsolete("Class no longer supported. Instead, please create a new instance of AutomationDriver.", false)]

    public class SpecFlowTestBase : TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecFlowTestBase" /> class.
        /// </summary>
        /// <param name="scenarioContext">The scenario context.</param>
        public SpecFlowTestBase(ScenarioContext scenarioContext)
            : base(false)
        {
            this.ScenarioContext = scenarioContext;

            if (!this.ScenarioContext.ContainsKey(nameof(this.AutomationDriver)))
            {
                this.AutomationDriver = AutomationDriverFactory.Get();
            }
        }

        #region Public properties

        /// <summary>
        /// Scenario Context.
        /// </summary>
        public virtual ScenarioContext ScenarioContext { get; set; }

        /// <summary>
        /// Gets or sets the Automation Driver.
        /// </summary>
        public override IAutomationDriver AutomationDriver
        {
            get
            {
                return this.ScenarioContext.GetValue<IAutomationDriver>(nameof(this.AutomationDriver));
            }

            set
            {
                this.ScenarioContext.SetValue(nameof(this.AutomationDriver), value);
            }
        }

        /// <summary>
        /// Gets or sets the Current Web Container.
        /// </summary>
        public override object CurrentWebContainer
        {
            get
            {
                return this.ScenarioContext.GetValue<object>(nameof(this.CurrentWebContainer));
            }

            protected set
            {
                this.ScenarioContext.SetValue(nameof(this.CurrentWebContainer), value);
            }
        }
        
        /// <summary>
        /// Gets the assembly where Web Objects are located.
        /// </summary>
        protected virtual Assembly WebObjectsAssembly
        {
            get
            {
                return null;
            }
        }

        #endregion

        #region Web Component

        /// <summary>
        /// Get Web Component by name.
        /// Important: property 'WebObjectsAssembly' must return the assembly where the Container is located.
        /// To use correct assembly, please override 'WebObjectsAssembly' property with 'Assembly.GetAssembly(typeof(YourClass))'.
        /// </summary>
        /// <param name="webContainerName">The name of container that contains component.</param>
        /// <param name="webComponentName">The name of component.</param>
        /// <returns>The Web Component.</returns>
        public IWebComponent GetWebComponent(string webContainerName, string webComponentName)
        {
            var assembly = this.WebObjectsAssembly ?? Assembly.GetCallingAssembly();
            return this.AutomationDriver.GetWebComponent(
                webContainerName,
                webComponentName,
                assembly);
        }

        #endregion
    }
}
