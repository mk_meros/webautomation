﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using System;
    using System.IO;
    using System.Reflection;
    using NUnit.Framework;
    using WebAutomation.Core.Tests.PageDefinitions;
    using WebAutomation.Core.WebObjects.Common;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    public class ScrollTests
    {
        public IAutomationDriver ATDriver = AutomationDriverFactory.Get();

        #region SetUp

        protected string TestPagePath
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uriBuilder = new UriBuilder(codeBase);
                string assemblyPath = Uri.UnescapeDataString(uriBuilder.Path);
                return Path.Combine(Path.GetDirectoryName(assemblyPath), "Resources", "ScrollTest.html");
            }
        }

        protected ScrollTestPage Page
        {
            get
            {
                return this.ATDriver.Get<ScrollTestPage>();
            }
        }

        [SetUp]
        public void BeforeTest()
        {
            this.ATDriver.Logger.Info($"Start test: {TestContext.CurrentContext.Test.FullName}");
            this.ATDriver.WebDriver = new ChromeDriver();
            this.ATDriver.WebDriver.Navigate().GoToUrl(this.TestPagePath);
        }

        [TearDown]
        public void AfterTest()
        {
            this.ATDriver.QuitBrowser(true);

            this.ATDriver.Logger.Info($"Test result: {TestContext.CurrentContext.Result.Outcome.Status}");
            this.ATDriver.Logger.Info("-----------");
        }

        #endregion


        [Test]
        public void ScrollToTopButtonBigMargin()
        {
            RegisterNewSettings(100, 100);
            Page.ScrollTopButton.Perform.Click();
            Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }

        [Test]
        public void ScrollToBottomButtonBigMargin()
        {
            RegisterNewSettings(100, 100);
            Page.ScrollBottomButton.Perform.Click();
            Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
        }

        [Test]
        public void ScrollToBottomAndThenTopButtonBigMargin()
        {
            RegisterNewSettings(100, 100);
            Page.ScrollBottomButton.WebElementProvider.WebElement.ScrollToMargin(this.ATDriver.WebDriver, 0, 0);

            Page.ScrollTopButton.Perform.Click();
            Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }



        [Test]
        public void ScrollToTopButtonMinMargin()
        {
            RegisterNewSettings(50, 50);
            Page.ScrollTopButton.Perform.Click();
            Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }

        [Test]
        public void ScrollToBottomButtonMinMargin()
        {
            RegisterNewSettings(50, 50);
            Page.ScrollBottomButton.Perform.Click();
            Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
        }

        [Test]
        public void ScrollToBottomAndThenTopButtonMinMargin()
        {
            RegisterNewSettings(50, 50);
            Page.ScrollBottomButton.WebElementProvider.WebElement.ScrollToMargin(this.ATDriver.WebDriver, 0, 0);

            Page.ScrollTopButton.Perform.Click();
            Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }



        [Test]
        public void ScrollToTopButtonNoMargin()
        {
            RegisterNewSettings(0, 0);
            Page.ScrollTopButton.Perform.Click();
            Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }

        [Test]
        public void ScrollToBottomButtonNoMargin()
        {
            RegisterNewSettings(0, 0);
            try
            {
                Page.ScrollBottomButton.Perform.Click();
                Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
                Assert.Fail("Button shouldn't be clickable");
            }
            catch (InvalidOperationException)
            {
                // OK
            }
            catch (WebDriverException)
            {
                // OK
            }
        }

        [Test]
        public void ScrollToBottomAndThenTopButtonNoMargin()
        {
            RegisterNewSettings(0, 0);
            Page.ScrollBottomButton.WebElementProvider.WebElement.ScrollToMargin(this.ATDriver.WebDriver, 0, 0);

            try
            {             
                Page.ScrollTopButton.Perform.Click();
                Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
                Assert.Fail("Button shouldn't be clickable");
            } catch (InvalidOperationException)
            {
                // OK
            } catch (WebDriverException)
            {
                // OK
            }
        }

        [Test]
        public void ScrollToTopButtonWithintMargin()
        {
            RegisterNewSettings(50, 50);

            // Scroll element to be hidden by top div
            IJavaScriptExecutor js = (IJavaScriptExecutor)this.ATDriver.WebDriver;
            var el = Page.ScrollTopButton.WebElementProvider.WebElement;
            int scrollPosition = int.Parse(js.ExecuteScript("return window.pageYOffset;").ToString());
            int offsetY = scrollPosition - el.Location.Y;
            js.ExecuteScript(string.Format("window.scrollBy(0,{0})", -offsetY));

            Page.ScrollTopButton.Perform.Click();
            Page.ScrollTopHeader.Assert.Has.Text("Top button clicked");
        }

        [Test]
        public void ScrollToBottomButtonWithingMargin()
        {
            RegisterNewSettings(50, 50);

            // Scroll element to be hidden by bottom div
            IJavaScriptExecutor js = (IJavaScriptExecutor)this.ATDriver.WebDriver;
            var el = Page.ScrollBottomButton.WebElementProvider.WebElement;
            int scrollPosition = int.Parse(js.ExecuteScript("return window.pageYOffset;").ToString());
            int browserInnerHeight = int.Parse(js.ExecuteScript("return window.innerHeight;").ToString());
            int offsetY = el.Location.Y + el.Size.Height - (scrollPosition + browserInnerHeight);
            js.ExecuteScript(string.Format("window.scrollBy(0,{0})", offsetY));

            Page.ScrollBottomButton.Perform.Click();
            Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
        }

        [Test]
        public void PerformScrollToBottomButtonWithingMargin()
        {
            var button = Page.ScrollBottomButton;
            var webElement = button.WebElementProvider.WebElement;
            webElement.ScrollToMargin(this.ATDriver.WebDriver, 0, 0);

            try
            {
                webElement.Click();
                Assert.Fail("Button shouldn't be clickable here.");
            } catch (Exception)
            {
                // OK
            }

            button.Perform.Scroll(50, 50);

            Assert.DoesNotThrow(() =>
            {
                webElement.Click();
            });

            Page.ScrollBottomHeader.Assert.Has.Text("Bottom button clicked");
        }

        private void RegisterNewSettings(int topScrollMargin, int bottomScrollMargin)
        {
            this.ATDriver.Settings = new Settings(4000, 400, topScrollMargin, bottomScrollMargin, 6, 500);
        }
    }
}
