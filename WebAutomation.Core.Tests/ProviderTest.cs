﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using NUnit.Framework;
    using OpenQA.Selenium;
    using WebAutomation.Core.Tests.PageDefinitions;

    public class ProviderTests : Test
    {
        [Test]
        public void ProviderById()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.TestIdAttributeSpan.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
            Assert.That(element.Text, Is.EqualTo("Test Provider"));
        }

        [Test]
        public void ProviderByPid()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.TestPidAttributeSpan.With("Id").WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
            Assert.That(element.Text, Is.EqualTo("Test Provider"));

            Assert.Throws<System.FormatException>(() =>
            {
                //// TODO: change implemention to throw better assertion
                var e = page.TestPidAttributeSpan.WebElementProvider.WebElement;
            });

            Assert.IsNull(page.TestPidAttributeSpan.With("test").WebElementProvider.WebElement);
        }

        [Test]
        public void ProviderByClass()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.TestClassAttributeSpan.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
            Assert.That(element.Text, Is.EqualTo("Test Provider"));
        }

        [Test]
        public void ProviderByPclass()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.TestPclassAttributeSpan.With("Class").WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
            Assert.That(element.Text, Is.EqualTo("Test Provider"));

            Assert.Throws<System.FormatException>(() =>
            {
                var e = page.TestPclassAttributeSpan.WebElementProvider.WebElement;
            });

            Assert.IsNull(page.TestPclassAttributeSpan.With("test").WebElementProvider.WebElement);
        }

        [Test]
        public void ProviderByCss()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.TestCssAttribute.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
            Assert.That(element.Text, Is.EqualTo("Test Provider"));
        }

        [Test]
        public void ProviderByPCss()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.TestPcssAttribute.With("Attribute").WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
            Assert.That(element.Text, Is.EqualTo("Test Provider"));
        }

        [Test]
        public void NestedWebComponent()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            page.NestedElement
                .Assert.Has.Text("Outside");

            page.NestedElement
                .In(page.ContainerForNestedElement.With("1"))
                .Assert.Has.Text("In container 1");

            page.NestedElement
                .In(page.ContainerForNestedElement.With("2"))
                .Assert.Has.Text("In container 2");

            page.NestedElement
                .In(page.ContainerForNestedElement.With("2"))
                .In(page.ContainerForNestedElement.With("1"))
                .Assert.Has.Text("In container 2");

            page.NestedElement
                .In(page.ContainerForNestedElement.With("1"))
                .In(page.ContainerForNestedElement.With("2"))
                .Assert.Is.NotPresent();
        }

        [Test]
        public void NestedWebComponentWithIframes()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            page.NestedElement
                .Assert.Has.Text("Outside");

            page.NestedElement
                .In(page.Iframe.With("11"))
                .Assert.Has.Text("In Iframe 1");

            page.NestedElement
                .Assert.Has.Text("Outside");

            page.NestedElement
                .In(page.Iframe.With("22"))
                .Assert.Is.NotPresent();

            page.NestedElement
                .In(page.Iframe.With("22"))
                .In(page.Iframe.With("11"))
                .Assert.Has.Text("In Iframe 2");
        }
    }
}
