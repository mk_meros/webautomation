﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using NUnit.Framework;
    using OpenQA.Selenium.Chrome;
    using WebAutomation.Core.Tests.PageDefinitions;

    public class SessionManagerTests : Test
    {
        [Test]
        public void CannotCreateNewSession()
        {
            this.ATDriver.Sessions.SwitchToDefaultSession();
            Assert.Throws<WebAutomationUsageException>(() => {
               base.ATDriver.Sessions.SwitchSession("xxx", false);
           });
        }

        [Test]
        public void CreateNewSession()
        {
            this.ATDriver.Sessions.SwitchToDefaultSession();
            Assert.That(this.ATDriver.WebDriver, Is.Not.Null);
            base.ATDriver.Sessions.SwitchSession("second");
            Assert.That(this.ATDriver.WebDriver, Is.Null);
        }

        [Test]
        public void SwitchSessions()
        {
            this.ATDriver.Sessions.SwitchToDefaultSession();
            Assert.That(base.ATDriver.Sessions.CurrentSessionName, Is.EqualTo("default"));
            base.ATDriver.Sessions.SwitchSession("second");
            Assert.That(base.ATDriver.Sessions.CurrentSessionName, Is.EqualTo("second"));
            base.ATDriver.Sessions.SwitchSession("third");
            Assert.That(base.ATDriver.Sessions.CurrentSessionName, Is.EqualTo("third"));
            base.ATDriver.Sessions.SwitchSession("second");
            Assert.That(base.ATDriver.Sessions.CurrentSessionName, Is.EqualTo("second"));
            base.ATDriver.Sessions.SwitchToDefaultSession();
            Assert.That(base.ATDriver.Sessions.CurrentSessionName, Is.EqualTo("default"));
        }

        [Test]
        public void AcctionsOnDifferentSessions()
        {
            this.ATDriver.Sessions.SwitchToDefaultSession();
            var page = this.ATDriver.Get<SampleWebSite>();
            page.EnabledButton.Assert.Has.Value("Enabled button");

            this.ATDriver.Sessions.SwitchSession("2");

            this.ATDriver.WebDriver = new ChromeDriver();
            this.ATDriver.WebDriver.Navigate().GoToUrl(this.TestPagePath);
            page = this.ATDriver.Get<SampleWebSite>();
            page.EnabledButton.Assert.Has.Value("Enabled button");
            page.EnabledButton.Perform.Click();
            page.EnabledButton.Assert.Has.Value("Enabled button clicked");

            this.ATDriver.Sessions.SwitchToDefaultSession();
            page = this.ATDriver.Get<SampleWebSite>();
            page.EnabledButton.Assert.Has.Value("Enabled button");
        }


    }
}
