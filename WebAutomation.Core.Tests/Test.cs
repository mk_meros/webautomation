﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using NUnit.Framework;
    using System;
    using System.IO;
    using System.Reflection;
    using OpenQA.Selenium.Chrome;

    public abstract class Test
    {
        public IAutomationDriver ATDriver;

        public Test()
        {
            this.ATDriver = AutomationDriverFactory.Get();
            this.ATDriver.WebDriver = new ChromeDriver();
            this.ATDriver.WebDriver.Navigate().GoToUrl(this.TestPagePath);
        }

        protected string TestPagePath
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uriBuilder = new UriBuilder(codeBase);
                string assemblyPath = Uri.UnescapeDataString(uriBuilder.Path);
                return Path.Combine(Path.GetDirectoryName(assemblyPath), "Resources", "Page1.html");
            }
        }

        [OneTimeTearDown]
        public void AfterAllTests()
        {
            this.ATDriver.QuitBrowser(true);
        }

        [SetUp]
        public void BeforeTest()
        {
            this.ATDriver.Logger.Info($"Start test: {TestContext.CurrentContext.Test.FullName}");
        }

        [TearDown]
        public void AfterTest()
        {  
            this.ATDriver.Logger.Info($"Test result: {TestContext.CurrentContext.Result.Outcome.Status}");
            this.ATDriver.Logger.Info("-----------");
        }
    }
}
