﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using WebAutomation.Core.Tests.PageDefinitions;
    using NUnit.Framework;

    public class StateTests : Test
    {
        #region Present

        [Test]
        public void StatePresent()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.TextSpan.Is.Present);
            Assert.That(page.TextSpan.WillBe.Present);
        }

        [Test]
        public void StateNotPresent()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.ElementNull.Is.NotPresent);
            Assert.That(page.ElementNull.WillBe.NotPresent);
        }

        [Test]
        public void StateWillNotPresent()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            page.ShowSpan1Button.Perform.Click();
            Assert.That(page.Span1.Is.Present);
            page.HideSpan1WithDelayButton.Perform.Click();
            Assert.That(page.Span1.WillBe.NotPresent);
        }

        [Test]
        public void StateWillPresent()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            page.HideSpan1Button.Perform.Click();
            Assert.That(page.Span1.Is.NotPresent);
            page.ShowSpan1WithDelayButton.Perform.Click();
            Assert.That(page.Span1.WillBe.Present);
        }

        #endregion

        #region Displayed

        [Test]
        public void StateDisplayed()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.TextSpan.Is.Displayed);
            Assert.That(page.TextSpan.WillBe.Displayed);
        }

        [Test]
        public void StateNotDisplayedNull()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.ElementNull.Is.NotDisplayed);
            Assert.That(page.ElementNull.WillBe.NotDisplayed);
        }

        [Test]
        public void StateNotDisplayedHidden()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.HiddenText.Is.NotDisplayed);
            Assert.That(page.HiddenText.WillBe.NotDisplayed);
        }

        [Test]
        public void StateWillNotDisplayed()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            page.ShowSpan1Button.Perform.Click();
            Assert.That(page.Span1.Is.Displayed);
            page.HideSpan1WithDelayButton.Perform.Click();
            Assert.That(page.Span1.WillBe.NotDisplayed);
        }

        [Test]
        public void StateWillDisplayed()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            page.HideSpan1Button.Perform.Click();
            Assert.That(page.Span1.Is.NotDisplayed);
            page.ShowSpan1WithDelayButton.Perform.Click();
            Assert.That(page.Span1.WillBe.Displayed);
        }

        #endregion

        #region Enabled

        [Test]
        public void StateEnabled()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.EnabledButton.Is.Enabled);
            Assert.That(page.EnabledButton.WillBe.Enabled);
        }

        [Test]
        public void StateNotEnabled()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.DisabledButton.Is.NotEnabled);
            Assert.That(page.DisabledButton.WillBe.NotEnabled);
        }

        #endregion

        #region Checked

        [Test]
        public void StateChecked()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.CheckboxChecked.Is.Checked);
            Assert.That(page.CheckboxChecked.WillBe.Checked);
        }

        [Test]
        public void StateNotChecked()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.CheckboxNotChecked.Is.NotChecked);
            Assert.That(page.CheckboxNotChecked.WillBe.NotChecked);
        }

        #endregion

        [Test]
        public void CustomState()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.That(page.EnabledButton.Is.InCorrectState((e) => e.Size.Width > 0, "Has correct width"));
            Assert.That(page.EnabledButton.WillBe.InCorrectState((e) => e.Size.Width > 0, "Has correct width"));
        }
    }
}
