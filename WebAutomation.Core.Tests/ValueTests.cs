﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using WebAutomation.Core.Tests.PageDefinitions;
    using WebAutomation.Core.WebObjects.WebComponents.Value;
    using NUnit.Framework;

    public class ValueTests : Test
    {
        [Test]
        public void ValueText()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.TextSpan.Has.Text("Sample text in span", StringType.Equals));
            Assert.That(page.TextSpan.WillHave.Text("Sample text in span", StringType.Equals));

            Assert.That(page.TextSpan.Has.Text("Sample text in", StringType.NotEquals));
            Assert.That(page.TextSpan.WillHave.Text("Sample text in", StringType.NotEquals));

            Assert.That(page.TextSpan.Has.Text("text in", StringType.Contains));
            Assert.That(page.TextSpan.WillHave.Text("text in", StringType.Contains));

            Assert.That(page.TextSpan.Has.Text("text in 2", StringType.NotContains));
            Assert.That(page.TextSpan.WillHave.Text("text in 2", StringType.NotContains));

            Assert.That(page.TextSpan.Has.Text("Sample text", StringType.StartsWith));
            Assert.That(page.TextSpan.WillHave.Text("Sample text", StringType.StartsWith));

            Assert.That(page.TextSpan.Has.Text("text in span", StringType.EndsWith));
            Assert.That(page.TextSpan.WillHave.Text("text in span", StringType.EndsWith));

            Assert.Throws<AssertionException>(() =>
            {
                Assert.That(page.TextSpan.Has.Text("Fake text", StringType.Equals));
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.TextSpan.Assert.Has.Text("Fake text", StringType.Equals);
            });
        }

        [Test]
        public void ValueValue()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.Textbox.Has.Value("Sample text in textbox", StringType.Equals));
            Assert.That(page.Textbox.WillHave.Value("Sample text in textbox", StringType.Equals));

            Assert.That(page.Textbox.Has.Value("Sample text in", StringType.NotEquals));
            Assert.That(page.Textbox.WillHave.Value("Sample text in", StringType.NotEquals));

            Assert.That(page.Textbox.Has.Value("text in", StringType.Contains));
            Assert.That(page.Textbox.WillHave.Value("text in", StringType.Contains));

            Assert.That(page.Textbox.Has.Value("text in 2", StringType.NotContains));
            Assert.That(page.Textbox.WillHave.Value("text in 2", StringType.NotContains));

            Assert.That(page.Textbox.Has.Value("Sample text", StringType.StartsWith));
            Assert.That(page.Textbox.WillHave.Value("Sample text", StringType.StartsWith));

            Assert.That(page.Textbox.Has.Value("text in textbox", StringType.EndsWith));
            Assert.That(page.Textbox.WillHave.Value("text in textbox", StringType.EndsWith));

            Assert.Throws<AssertionException>(() =>
            {
                Assert.That(page.Textbox.Has.Value("Fake value", StringType.Equals));
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.Textbox.Assert.Has.Value("Fake value", StringType.Equals);
            });
        }

        [Test]
        public void ValueTextLength()
        {
            int length = "Sample text in span".Length;

            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.TextSpan.Has.TextLength(length, NumberType.Equals));
            Assert.That(page.TextSpan.WillHave.TextLength(length, NumberType.Equals));

            Assert.That(page.TextSpan.Has.TextLength(length + 1, NumberType.NotEquals));
            Assert.That(page.TextSpan.WillHave.TextLength(length + 1, NumberType.NotEquals));

            Assert.That(page.TextSpan.Has.TextLength(length + 1, NumberType.LessThan));
            Assert.That(page.TextSpan.WillHave.TextLength(length + 1, NumberType.LessThan));

            Assert.That(page.TextSpan.Has.TextLength(length + 1, NumberType.LessThanOrEqualTo));
            Assert.That(page.TextSpan.WillHave.TextLength(length + 1, NumberType.LessThanOrEqualTo));

            Assert.That(page.TextSpan.Has.TextLength(length, NumberType.LessThanOrEqualTo));
            Assert.That(page.TextSpan.WillHave.TextLength(length, NumberType.LessThanOrEqualTo));

            Assert.That(page.TextSpan.Has.TextLength(length - 1, NumberType.GreaterThan));
            Assert.That(page.TextSpan.WillHave.TextLength(length - 1, NumberType.GreaterThan));

            Assert.That(page.TextSpan.Has.TextLength(length - 1, NumberType.GreaterThanOrEqualTo));
            Assert.That(page.TextSpan.WillHave.TextLength(length - 1, NumberType.GreaterThanOrEqualTo));

            Assert.That(page.TextSpan.Has.TextLength(length, NumberType.GreaterThanOrEqualTo));
            Assert.That(page.TextSpan.WillHave.TextLength(length, NumberType.GreaterThanOrEqualTo));

            Assert.Throws<AssertionException>(() =>
            {
                Assert.That(page.TextSpan.Has.TextLength(length + 1, NumberType.Equals));
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.TextSpan.Assert.Has.TextLength(length + 1, NumberType.Equals);
            });
        }

        [Test]
        public void ValueValueLength()
        {
            int length = "Sample text in textbox".Length;

            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.Textbox.Has.ValueLength(length, NumberType.Equals));
            Assert.That(page.Textbox.WillHave.ValueLength(length, NumberType.Equals));

            Assert.That(page.Textbox.Has.ValueLength(length + 1, NumberType.NotEquals));
            Assert.That(page.Textbox.WillHave.ValueLength(length + 1, NumberType.NotEquals));

            Assert.That(page.Textbox.Has.ValueLength(length + 1, NumberType.LessThan));
            Assert.That(page.Textbox.WillHave.ValueLength(length + 1, NumberType.LessThan));

            Assert.That(page.Textbox.Has.ValueLength(length + 1, NumberType.LessThanOrEqualTo));
            Assert.That(page.Textbox.WillHave.ValueLength(length + 1, NumberType.LessThanOrEqualTo));

            Assert.That(page.Textbox.Has.ValueLength(length, NumberType.LessThanOrEqualTo));
            Assert.That(page.Textbox.WillHave.ValueLength(length, NumberType.LessThanOrEqualTo));

            Assert.That(page.Textbox.Has.ValueLength(length - 1, NumberType.GreaterThan));
            Assert.That(page.Textbox.WillHave.ValueLength(length - 1, NumberType.GreaterThan));

            Assert.That(page.Textbox.Has.ValueLength(length - 1, NumberType.GreaterThanOrEqualTo));
            Assert.That(page.Textbox.WillHave.ValueLength(length - 1, NumberType.GreaterThanOrEqualTo));

            Assert.That(page.Textbox.Has.ValueLength(length, NumberType.GreaterThanOrEqualTo));
            Assert.That(page.Textbox.WillHave.ValueLength(length, NumberType.GreaterThanOrEqualTo));

            Assert.Throws<AssertionException>(() =>
            {
                Assert.That(page.Textbox.Has.ValueLength(length + 1, NumberType.Equals));
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.Textbox.Assert.Has.ValueLength(length + 1, NumberType.Equals);
            });
        }

        [Test]
        public void ValueOption()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.That(page.Select.Has.Option("Option 1", OptionType.Selected));
            Assert.That(page.Select.WillHave.Option("Option 1", OptionType.Selected));

            Assert.That(page.Select.Has.Option("Option 2", OptionType.NotSelected));
            Assert.That(page.Select.WillHave.Option("Option 2", OptionType.NotSelected));

            Assert.That(page.Select.Has.Option("Option 3", OptionType.Available));
            Assert.That(page.Select.WillHave.Option("Option 3", OptionType.Available));

            Assert.That(page.Select.Has.Option("Option 4", OptionType.NotAvailable));
            Assert.That(page.Select.WillHave.Option("Option 4", OptionType.NotAvailable));

            Assert.Throws<AssertionException>(() =>
            {
                Assert.That(page.Select.Has.Option("Option 2", OptionType.Selected));
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.Select.Assert.Has.Option("Option 2", OptionType.Selected);
            });
        }

        [Test]
        public void ValueCss()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.That(page.DivWithBackground.Has.CssValue("background-color", "rgba(255, 0, 0, 1)", StringType.Equals));
            Assert.That(page.DivWithBackground.WillHave.CssValue("background-color", "rgba(255, 0, 0, 1)", StringType.Equals));

            Assert.That(page.DivWithBackground.Has.CssValue("background-color", "rgba(255, 0, 0, 2)", StringType.NotEquals));
            Assert.That(page.DivWithBackground.WillHave.CssValue("background-color", "rgba(255, 0, 0, 2)", StringType.NotEquals));

            Assert.That(page.DivWithBackground.Has.CssValue("background-color", "255, 0, 0", StringType.Contains));
            Assert.That(page.DivWithBackground.WillHave.CssValue("background-color", "255, 0, 0", StringType.Contains));

            Assert.That(page.DivWithBackground.Has.CssValue("background-color", "255, 0, 1", StringType.NotContains));
            Assert.That(page.DivWithBackground.WillHave.CssValue("background-color", "255, 0, 1", StringType.NotContains));

            Assert.That(page.DivWithBackground.Has.CssValue("background-color", "rgba", StringType.StartsWith));
            Assert.That(page.DivWithBackground.WillHave.CssValue("background-color", "rgba", StringType.StartsWith));

            Assert.That(page.DivWithBackground.Has.CssValue("background-color", "1)", StringType.EndsWith));
            Assert.That(page.DivWithBackground.WillHave.CssValue("background-color", "1)", StringType.EndsWith));

            Assert.Throws<AssertionException>(() =>
            {
                Assert.That(page.DivWithBackground.Has.CssValue("background-color", "Fake value", StringType.Equals));
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.DivWithBackground.Assert.Has.CssValue("background-color", "Fake value", StringType.Equals);
            });
        }

        [Test]
        public void ValueAttribute()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.That(page.TextSpan.Has.AttributeValue("class", "myClass", StringType.Equals));
            Assert.That(page.TextSpan.WillHave.AttributeValue("class", "myClass", StringType.Equals));

            Assert.That(page.TextSpan.Has.AttributeValue("fakeAttr", "test", StringType.NotEquals));
            Assert.That(page.TextSpan.WillHave.AttributeValue("fakeAttr", "test", StringType.NotEquals));

            Assert.Throws<TestAssertionException>(() =>
            {
                page.TextSpan.Assert.Has.AttributeValue("background-color", "Fake value", StringType.Equals);
            });
        }

        [Test]
        public void ValueAttributeWillHave()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            page.SetAttrValue1Button.Perform.Click();
            Assert.That(page.DivWithBackground.Has.AttributeValue("class", "test1", StringType.Equals));

            page.SetAttrValueWithDelay2Button.Perform.Click();
            Assert.That(page.DivWithBackground.WillHave.AttributeValue("class", "test2", StringType.Equals));

            page.SetAttrValueWithDelay1Button.Perform.Click();
            Assert.False(page.DivWithBackground.Has.AttributeValue("class", "test1", StringType.Equals));
            Assert.That(page.DivWithBackground.WillHave.AttributeValue("class", "test1", StringType.Equals));

        }

        [Test]
        public void ElementsNumber()
        {
            int number = 3;

            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.Options1.Has.ElementsNumber(number, NumberType.Equals));
            Assert.That(page.Options1.WillHave.ElementsNumber(number, NumberType.Equals));

            Assert.That(page.Options1.Has.ElementsNumber(number + 1, NumberType.NotEquals));
            Assert.That(page.Options1.WillHave.ElementsNumber(number + 1, NumberType.NotEquals));

            Assert.That(page.Options1.Has.ElementsNumber(number + 1, NumberType.LessThan));
            Assert.That(page.Options1.WillHave.ElementsNumber(number + 1, NumberType.LessThan));

            Assert.That(page.Options1.Has.ElementsNumber(number + 1, NumberType.LessThanOrEqualTo));
            Assert.That(page.Options1.WillHave.ElementsNumber(number + 1, NumberType.LessThanOrEqualTo));

            Assert.That(page.Options1.Has.ElementsNumber(number, NumberType.LessThanOrEqualTo));
            Assert.That(page.Options1.WillHave.ElementsNumber(number, NumberType.LessThanOrEqualTo));

            Assert.That(page.Options1.Has.ElementsNumber(number - 1, NumberType.GreaterThan));
            Assert.That(page.Options1.WillHave.ElementsNumber(number - 1, NumberType.GreaterThan));

            Assert.That(page.Options1.Has.ElementsNumber(number - 1, NumberType.GreaterThanOrEqualTo));
            Assert.That(page.Options1.WillHave.ElementsNumber(number - 1, NumberType.GreaterThanOrEqualTo));

            Assert.That(page.Options1.Has.ElementsNumber(number, NumberType.GreaterThanOrEqualTo));
            Assert.That(page.Options1.WillHave.ElementsNumber(number, NumberType.GreaterThanOrEqualTo));

            Assert.Throws<AssertionException>(() =>
            {
                Assert.That(page.Options1.Has.ElementsNumber(number + 1, NumberType.Equals));
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.Options1.Assert.Has.ElementsNumber(number + 1, NumberType.Equals);
            });
        }

        [Test]
        public void ElementsSorted()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            // 3 elements already sorted
            Assert.That(page.Options1.Has.ElementsSortedByText());
            Assert.That(page.Options1.WillHave.ElementsSortedByText());

            // 1 element
            Assert.That(page.TextSpan.Has.ElementsSortedByText());
            Assert.That(page.TextSpan.WillHave.ElementsSortedByText());

            Assert.Throws<AssertionException>(() =>
            {
                Assert.That(page.Options2.Has.ElementsSortedByText());
            });
        }


        [Test]
        public void CustomValue()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.That(page.TextSpan.Has.CorrectValue((e) => e.Size.Width > 0, "Is visible"));
            Assert.That(page.TextSpan.WillHave.CorrectValue((e) => e.Size.Width > 0, "Is visible"));
        }
    }
}
