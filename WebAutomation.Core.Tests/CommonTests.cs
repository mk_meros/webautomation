﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using WebAutomation.Core.Tests.PageDefinitions;
    using WebAutomation.Core.WebObjects.WebComponents;
    using NUnit.Framework;
    using OpenQA.Selenium;

    [TestFixture]
    public class CommonTests : Test
    {

        [Test]
        public void Get()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            var page2 = this.ATDriver.Get<SampleWebSite>();
            Assert.That(!page.Equals(page2));
        }

        [Test]
        public void WebElementProviderAlreadyDefined()
        {
            Assert.Throws<WebAutomationUsageException>(() =>
            {
                var test = this.ATDriver.Get<TooManyWebElementProviderAttributes>();
            });
        }

        [Test]
        public void MissingWebElementProvider()
        {
            var page = this.ATDriver.Get<MissingWebElementProviderAttribute>();
            Assert.That(page.WithoutAttributes.WebElementProvider, Is.Null);
            Assert.Throws<WebAutomationUsageException>(() =>
                {
                    var test = page.WithoutAttributes.Has;
                });

            Assert.Throws<WebAutomationUsageException>(() =>
                {
                    var test = page.WithoutAttributes.WillHave;
                });

            Assert.Throws<WebAutomationUsageException>(() =>
                {
                    var test = page.WithoutAttributes.Is;
                });

            Assert.Throws<WebAutomationUsageException>(() =>
                {
                    var test = page.WithoutAttributes.WillBe;
                });

            Assert.Throws<WebAutomationUsageException>(() =>
                {
                    var test = page.WithoutAttributes.Perform;
                });

            Assert.Throws<WebAutomationUsageException>(() =>
                {
                    var test = page.WithoutAttributes.PerformIfExists;
                });
        }

        [Test]
        public void GetByXPath()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.Paragraph.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
        }

        [Test]
        public void GetByPXPath()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.ParagraphByText.With("Sample html page 1").WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
        }

        [Test]
        public void GetByXPathNull()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.ElementNull.WebElementProvider.WebElement;
            Assert.That(element, Is.Null);
        }

        [Test]
        public void GetByPXPathNull()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.ParagraphByText.With("Wrong text").WebElementProvider.WebElement;
            Assert.That(element, Is.Null);
        }

        [Test]
        public void GetValueOnNullElementByPXPath()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.Throws<TestAssertionException>(() =>
                page.ParagraphByText.With("Wrong text", "wrong param").Assert.Has.Text("fake text")
            );
        }

        [Test]
        public void CheckIfDisplayedWithWrongParameters()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.Throws<TestAssertionException>(() =>
                page.TableRow.With("Row 1 C 1", "Row 1 C 2", "Row 1 C 3", "Row 1 C 5").Assert.Is.Displayed()
            );
        }

        [Test]
        public void GetByNestedXPath()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.ParagraphNestedDefinitionElement.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
        }

        [Test]
        public void GetByNestedPXPath()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            IWebElement element = page.ParagraphNestedDefinitionByTextElement.With("Sample html page 1").WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
        }

        [Test]
        public void IFrameContainerGetElement()
        {
            var page = this.ATDriver.Get<HomePage>();
            IWebElement element = page.BoldText.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
        }

        [Test]
        public void IFrameContainerGetElementOutsideIframe()
        {
            var page = this.ATDriver.Get<HomePage>();
            IWebElement element = page.Paragraph.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);
        }

        [Test]
        public void IFrameContainerGetVariousElement()
        {
            var page = this.ATDriver.Get<HomePage>();
            IWebElement element = page.Paragraph.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);

            IWebElement element2 = page.BoldText.WebElementProvider.WebElement;
            Assert.That(element2, Is.Not.Null);

            IWebElement element3 = page.ParagraphNestedDefinitionElement.WebElementProvider.WebElement;
            Assert.That(element3, Is.Not.Null);
        }

        [Test]
        public void NestedIFrameContainerGetElement()
        {
            var page = this.ATDriver.Get<HomeNewsPage>();
            IWebElement element = page.Div.WebElementProvider.WebElement;
            Assert.That(element, Is.Not.Null);

            page.DivSpan1.Assert.Has.Text("test");
        }

        [Test]
        public void IncorrectIframeGetElement()
        {
            this.ATDriver.Get<IncorrectIframe>().IncorrectIframeElement.Assert.Is.NotDisplayed();
        }

        [Test]
        public void OnLoad()
        {
            Assert.DoesNotThrow(() =>
                {
                    this.ATDriver.Get<HomeNewsTestPage>();
                });
        }

        [Test]
        public void AssertNotPresent()
        {
            Assert.DoesNotThrow(() =>
            {
                this.ATDriver.Get<TestNotPresentPage>();
            });
        }

        [Test]
        public void AssertNotPresentNegative()
        {
            Assert.Throws<TestAssertionException>(() =>
            {
                this.ATDriver.Get<TestNotPresentNegativePage>();
            });
        }

        [Test]
        public void OnLoadNegative()
        {
            Assert.Throws<TestAssertionException>(() =>
            {
                this.ATDriver.Get<OnLoadTest>();
            });
        }

        [Test]
        public void ActionsTest()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.Paragraph.Perform.Click();
        }

        [Test]
        public void ActionsOptionalTest()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.Paragraph.PerformIfExists.Click();
        }

        [Test]
        public void ActionsElementNotExists()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.Throws<TestAssertionException>(() =>
            {
                page.ElementNull.Perform.Click();
            });
        }

        [Test]
        public void ActionsOptionalElementNotExists()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.ElementNull.PerformIfExists.Click();
        }

        [Test]
        public void StatePositive()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            Assert.That(page.Paragraph.Is.Present);
            Assert.That(page.Paragraph.Is.Displayed);
            Assert.That(page.Paragraph.Is.Enabled);

            Assert.That(page.Paragraph.Is.NotPresent, Is.False);
            Assert.That(page.Paragraph.Is.NotDisplayed, Is.False);
        }

        [Test]
        public void StateNegative()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.That(page.ElementNull.Is.Present, Is.False);
            Assert.That(page.ElementNull.Is.Displayed, Is.False);

            Assert.That(page.ElementNull.Is.NotPresent);
            Assert.That(page.ElementNull.Is.NotDisplayed);
        }

        [Test]
        public void StateException()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.Throws<TestAssertionException>(() =>
            {
                bool result = page.ElementNull.Is.Enabled;
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                bool result = page.ElementNull.Is.NotEnabled;
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                bool result = page.ElementNull.Is.Checked;
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                bool result = page.ElementNull.Is.NotChecked;
            });
        }

        [Test]
        public void AssertState()
        {
            var page = this.ATDriver.Get<SampleWebSite>();

            Assert.Throws<TestAssertionException>(() =>
            {
                page.ElementNull.Assert.Is.Present();
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.ElementNull.Assert.Is.InCorrectState((e) => e != null, "Not null");
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.ElementNull.Assert.Has.CorrectValue((e) => e != null, "Not null");
            });

            Assert.Throws<TestAssertionException>(() =>
            {
                page.ElementNull.Assert.Is.Displayed();
            });

            Assert.DoesNotThrow(() =>
            {
                page.ElementNull.Assert.Is.NotPresent();
            });

            Assert.DoesNotThrow(() =>
            {
                page.ElementNull.Assert.Is.NotDisplayed();
            });
        }

        [Test]
        public void LoggerTestPositive()
        {
            this.ATDriver.Logger.Info("Sample log {test}");

            this.ATDriver.Logger.Debug("Sample log");

            this.ATDriver.Logger.Warn("Sample log");

            this.ATDriver.Logger.Error("Sample log");

            this.ATDriver.Logger.Info(null);
        }
    }
}
