﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Tests
{
    using NUnit.Framework;
    using WebAutomation.Core.Tests.PageDefinitions;
    using WebAutomation.Core.WebObjects.WebComponents.Actions;

    public class PerformTests : Test
    {
        [Test]
        public void PerformClick()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.EnabledButton.Perform.Click();
            Assert.That(page.EnabledButton.Has.Value("Enabled button clicked"));
        }

        [Test]
        public void PerformRightClick()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.RightClickDiv.Perform.Click(ClickType.Right);
            Assert.That(page.RightClickDiv.Has.Text("clicked"));
        }

        [Test]
        public void PerformDragAndDrop()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.DropDivWitContent.Assert.Is.NotPresent();
            page.DragContent.Perform.DragAndDrop(page.DropDiv);
            page.DropDivWitContent.Assert.Is.Present();
        }

        [Test]
        public void PerformFill()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.TextboxRandom.Perform.Fill("New value");
            Assert.That(page.TextboxRandom.Has.Value("New value"));
        }

        [Test]
        public void PerformClear()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.TextboxRandom.Perform.Fill("New value");
            page.TextboxRandom.Perform.Clear();
            Assert.That(page.TextboxRandom.Has.Value(""));
        }

        [Test]
        public void PerformCheck()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.Checkbox.Perform.Check();
            Assert.That(page.Checkbox.Is.Checked);

            page.Checkbox.Perform.Check();
            Assert.That(page.Checkbox.Is.Checked);

            page.Checkbox.Perform.Uncheck();
            Assert.That(page.Checkbox.Is.NotChecked);

            page.Checkbox.Perform.Uncheck();
            Assert.That(page.Checkbox.Is.NotChecked);
        }

        [Test]
        public void PerformSelect()
        {
            var page = this.ATDriver.Get<SampleWebSite>();
            page.SelectRandom.Perform.Select("Option 2");
            Assert.That(page.SelectRandom.Has.Option("Option 2"));

            page.SelectRandom.Perform.Select(0);
            Assert.That(page.SelectRandom.Has.Option("Option 1"));
        }

        [Test]
        public void PerformIfExists()
        {
            Assert.DoesNotThrow(() =>
            {
                var page = this.ATDriver.Get<SampleWebSite>();
                page.ElementNull.PerformIfExists.Click();
                page.ElementNull.PerformIfExists.Check();
                page.ElementNull.PerformIfExists.Clear();
                page.ElementNull.PerformIfExists.Fill("test");
                page.ElementNull.PerformIfExists.Hover();
                page.ElementNull.PerformIfExists.Select(2);
                page.ElementNull.PerformIfExists.Select("test");
                page.ElementNull.PerformIfExists.Uncheck();
            });
        }
    }
}
