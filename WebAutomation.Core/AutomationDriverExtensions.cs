﻿//-----------------------------------------------------------------------
// Copyright (c) 2017 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using System.Linq;
    using System.Reflection;
    using WebAutomation.Core.WebObjects.WebComponents;

    /// <summary>
    /// AutomationDriver extensions.
    /// </summary>
    public static class AutomationDriverExtensions
    {
        /// <summary>
        /// Get Web Component by using name of WebContainer and WebComponent. 
        /// Use this method ONLY if you cannot use Strongly-typed approach (see Get[T] method).
        /// </summary>
        /// <param name="automationDriver">The AutomationDriver.</param>
        /// <param name="containerName">The Web Container name.</param>
        /// <param name="componentName">The Web Component name.</param>
        /// <param name="assembly">Assembly that contains the container. To get the assembly you can use: 'typeof(TypeInAssembly).Assembly'.</param>
        /// <returns>Web Component.</returns>
        public static IWebComponent GetWebComponent(
            this IAutomationDriver automationDriver, 
            string containerName, 
            string componentName, 
            Assembly assembly)
        {
            AssertHelper.AssertUsage(
                !string.IsNullOrEmpty(componentName), 
                "Web Component name cannot be empty", 
                automationDriver.Logger);

            var container = automationDriver.GetWebContainer(containerName, assembly);
            var webComponentPropertyInfo = container
                .GetType()
                .GetProperty(componentName, typeof(IWebComponent));

            AssertHelper.AssertUsage(
                webComponentPropertyInfo != null,
                $"Cannot find WebComponent '{componentName}' inside '{containerName}' class. Please ensure that the name of WebComponent is correct and exists in given WebContainer.",
                automationDriver.Logger);

            return (IWebComponent)webComponentPropertyInfo.GetValue(container);
        }

        /// <summary>
        /// Get Web Container by using the name of class.
        /// Use this method ONLY if you cannot use Strongly-typed approach (see Get[T]() method).
        /// </summary>
        /// <param name="automationDriver">The AutomationDriver.</param>
        /// <param name="containerName">The Web Container name.</param>
        /// <param name="assembly">Assembly that contains the container. To get the assembly you can use: 'typeof(TypeInAssembly).Assembly'.</param>
        /// <returns>Web Container.</returns>
        public static object GetWebContainer(
            this IAutomationDriver automationDriver, 
            string containerName,
            Assembly assembly)
        {
            AssertHelper.AssertUsage(
                !string.IsNullOrEmpty(containerName), 
                "Web Container name cannot be empty", 
                automationDriver.Logger);

            AssertHelper.AssertUsage(
                assembly != null,
                "Assembly cannot be null",
                automationDriver.Logger);

            var webContainerType = assembly
                .ExportedTypes
                .FirstOrDefault(t => t.Name.Equals(containerName));

            AssertHelper.AssertUsage(
                webContainerType != null,
                $"Cannot find WebContainer '{containerName}' in assembly '{assembly.FullName}'.",
                automationDriver.Logger);

            return automationDriver.Get(webContainerType);
        }
    }
}
