﻿//-----------------------------------------------------------------------
// Copyright (c) 2017 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using System;

    using Microsoft.Practices.Unity;
    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;
    using WebAutomation.Core.WebObjects.Manager;

    /// <summary>
    /// The AutomationDriver.
    /// </summary>
    internal class AutomationDriver : IAutomationDriver
    {
        /// <summary>
        /// Dependency container.
        /// </summary>
        private IUnityContainer container;

        /// <summary>
        /// Web Object Manager.
        /// </summary>
        private IWebObjectsManager webObjectManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutomationDriver" /> class.
        /// </summary>
        public AutomationDriver()
        {
            this.container = DependencyContainer.InitContainer();
            this.Sessions = this.container.Resolve<ISessionManager>();
            this.Extensions = this.container.Resolve<IExtensionsManager>();
            this.webObjectManager = this.container.Resolve<IWebObjectsManager>();
        }

        /// <summary>
        /// Gets the session manager.
        /// </summary>
        public ISessionManager Sessions { get; private set; }

        /// <summary>
        /// Gets the extensions manager.
        /// </summary>
        public IExtensionsManager Extensions { get; private set; }

        /// <summary>
        /// Gets or sets the WebDriver for the current session (see Sessions.CurrentSession).
        /// </summary>
        public IWebDriver WebDriver
        {
            get
            {
                return this.Sessions.CurrentSession;
            }

            set
            {
                this.Sessions.CurrentSession = value;
            }
        }

        /// <summary>
        /// Gets or sets the Settings.
        /// </summary>
        public ISettings Settings
        {
            get
            {
                return this.container.Resolve<ISettings>();
            }

            set
            {
                this.container.RegisterInstance<ISettings>(value);
            }
        }

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        public ILogger Logger
        {
            get
            {
                return this.container.Resolve<ILogger>();
            }

            set
            {
                this.container.RegisterInstance<ILogger>(value);
            }
        }

        /// <summary>
        /// Get a Web Container.
        /// </summary>
        /// <typeparam name="TWebContainer">Type of Web Container.</typeparam>
        /// <returns>The Web Container.</returns>
        public TWebContainer Get<TWebContainer>() where TWebContainer : new()
        {
            return this.webObjectManager.Load<TWebContainer>(this.Sessions.CurrentSession);
        }

        /// <summary>
        /// Get a Web Container.
        /// </summary>
        /// <param name="webContainerType">Type of Web Container.</param>
        /// <returns>The Web Container.</returns>
        public object Get(Type webContainerType)
        {
            return this.webObjectManager.Load(webContainerType, this.Sessions.CurrentSession);
        }

        /// <summary>
        /// Quit browser.
        /// </summary>
        /// <param name="quitBrowsersInAllSessions">Quit browser in all sessions.</param>
        public void QuitBrowser(bool quitBrowsersInAllSessions = false)
        {
            if (quitBrowsersInAllSessions)
            {
                foreach (var session in this.Sessions.AvailableSessionsNames)
                {
                    this.Logger.Info("Switch session to: " + session);
                    this.Sessions.SwitchSession(session, false);
                    this.QuitBrowserFromCurrentSession();
                }
            }
            else
            {
                this.QuitBrowserFromCurrentSession();
            }
        }

        /// <summary>
        /// Quit browser from current session.
        /// </summary>
        private void QuitBrowserFromCurrentSession()
        {
            if (this.Sessions.CurrentSession != null)
            {
                try
                {
                    this.Logger.Info("Quit browser");
                    this.Sessions.CurrentSession.Quit();               
                }
                catch (Exception ex)
                {
                    this.Logger.Warn($"Cannot quit browser. Details: {ex}");
                }
                finally
                {
                    this.Sessions.CurrentSession = null;
                }
            }
        }
    }
}
