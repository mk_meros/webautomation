﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.WebElementProviders
{
    using System.Collections.Generic;
    using OpenQA.Selenium;

    /// <summary>
    /// Web Element provider.
    /// Defines the way how Web Element should be find by Web Driver.
    /// </summary>
    public interface IWebElementProvider
    {
        /// <summary>
        /// Gets or sets the name of Web Component.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the Web Driver.
        /// </summary>
        IWebDriver WebDriver { get; set; }

        /// <summary>
        /// Gets or sets the Web Element Provider for the parent element.
        /// </summary>
        IWebElementProvider ParentWebElementProvider { get; set; }

        /// <summary>
        /// Gets or sets the search expression.
        /// </summary>
        string SearchExpression { get; set; }

        /// <summary>
        /// Gets or sets the additional search parameters.
        /// </summary>
        string[] SearchParameters { get; set; }

        /// <summary>
        /// Gets the Web Element.
        /// </summary>
        IWebElement WebElement { get; }

        /// <summary>
        /// Gets the list of Web Elements.
        /// </summary>
        IList<IWebElement> WebElements { get; }

        /// <summary>
        /// Clone current Web Element provider.
        /// </summary>
        /// <returns>New copy of current Web Element provider.</returns>
        IWebElementProvider Clone();
    }
}
