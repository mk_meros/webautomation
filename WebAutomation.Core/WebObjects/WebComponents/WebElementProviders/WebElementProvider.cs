﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.WebElementProviders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common;
    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Base class for Web Element providers.
    /// </summary>
    public abstract class WebElementProvider : IWebElementProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WebElementProvider" /> class.
        /// </summary>
        public WebElementProvider()
        {
            this.Name = string.Empty;
            this.SearchExpression = string.Empty;
            this.SearchParameters = new string[0];
        }

        /// <summary>
        /// Gets or sets the name of Web Component.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Web Driver.
        /// </summary>
        public IWebDriver WebDriver { get; set; }

        /// <summary>
        /// Gets or sets the Web Element Provider for the parent element.
        /// </summary>
        public IWebElementProvider ParentWebElementProvider { get; set; }

        /// <summary>
        /// Gets the Web Element.
        /// </summary>
        public IWebElement WebElement
        {
            get
            {
                if (this.ParentWebElementProvider == null)
                {
                    this.WebDriver.SwitchTo().DefaultContent();
                    return this.FindElementIn(this.WebDriver);
                }

                var parentWebElement = this.ParentWebElementProvider.WebElement;
                if (parentWebElement == null)
                {
                    //// TODO: provide additional information in logs
                    return null;
                }

                if (parentWebElement.IsIFrame())
                {
                    this.WebDriver.SwitchTo().Frame(parentWebElement);
                    return this.FindElementIn(this.WebDriver);
                }

                return this.FindElementIn(parentWebElement);
            }
        }

        /// <summary>
        /// Gets the list of Web Elements.
        /// </summary>
        public IList<IWebElement> WebElements
        {
            get
            {
                if (this.ParentWebElementProvider == null)
                {
                    this.WebDriver.SwitchTo().DefaultContent();
                    return this.FindElementsIn(this.WebDriver);
                }

                var parentWebElement = this.ParentWebElementProvider.WebElement;
                if (parentWebElement == null)
                {
                    //// TODO: add warning to logs
                    return null;
                }

                if (parentWebElement.IsIFrame())
                {
                    this.WebDriver.SwitchTo().Frame(parentWebElement);
                    return this.FindElementsIn(this.WebDriver);
                }

                return this.FindElementsIn(parentWebElement);
            }
        }

        /// <summary>
        /// Gets or sets the search expression.
        /// </summary>
        public string SearchExpression { get; set; }

        /// <summary>
        /// Gets or sets the additional search parameters.
        /// </summary>
        public string[] SearchParameters { get; set; }

        /// <summary>
        /// Gets the full search expression (based on Search Expression and Search Parameters).
        /// </summary>
        protected virtual string FullSearchExpression
        {
            get
            {
                return this.SearchExpression;
            }
        }

        /// <summary>
        /// Gets the full search expression that should be used when searching inside another component.
        /// </summary>
        protected virtual string FullSearchExpressionForNestedComponent
        {
            get
            {
                return this.FullSearchExpression;
            }
        }

        /// <summary>
        /// Gets the function that returns By object which is used for searching the Web Element.
        /// </summary> 
        protected abstract Func<string, By> SearchBy { get; }

        /// <summary>
        /// Gets the search method.
        /// </summary>
        protected virtual string SearchMethod
        {
            get
            {
                return "id";
            }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>String representation of the current object.</returns>
        public override string ToString()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("Name", this.Name);
            parameters.Add("Search by", this.SearchMethod);

            if (this.SearchParameters != null && this.SearchParameters.Length > 0)
            {
                string searchParameters = this.SearchParameters
                                        .Select(p => "'" + p + "'")
                                        .Aggregate<string>((p1, p2) => p1 + ", " + p2);

                parameters.Add("Parameters", searchParameters);
            }

            parameters.Add("Expression", this.FullSearchExpression);

            var parentInfo = this.ParentWebElementProvider != null
                ? LogFormatter.GetSecondLevelLog(this.ParentWebElementProvider.ToString())
                : string.Empty;

            return LogFormatter.GetLog(parameters) + parentInfo;
        }

        /// <summary>
        /// Clone current Web Element provider.
        /// </summary>
        /// <returns>New copy of current Web Element provider.</returns>
        public virtual IWebElementProvider Clone()
        {
            IWebElementProvider webElementProvider = (IWebElementProvider)Activator.CreateInstance(this.GetType());
            webElementProvider.Name = this.Name;
            webElementProvider.WebDriver = this.WebDriver;
            webElementProvider.ParentWebElementProvider = this.ParentWebElementProvider?.Clone();
            webElementProvider.SearchExpression = this.SearchExpression;
            webElementProvider.SearchParameters = this.SearchParameters;

            return webElementProvider;
        }

        /// <summary>
        /// Find element in the Web Driver.
        /// </summary>
        /// <param name="webDriver">The Web Driver.</param>
        /// <returns>Web element or null.</returns>
        private IWebElement FindElementIn(IWebDriver webDriver)
        {
            return this.FindElementIn(
                webDriver, 
                this.SearchBy(this.FullSearchExpression));
        }

        /// <summary>
        /// Find element in another element.
        /// </summary>
        /// <param name="webElement">The Web Element.</param>
        /// <returns>Web element or null.</returns>
        private IWebElement FindElementIn(IWebElement webElement)
        {
            return this.FindElementIn(
                webElement,
                this.SearchBy(this.FullSearchExpressionForNestedComponent));
        }

        /// <summary>
        /// Find element in the Web Driver.
        /// </summary>
        /// <param name="webDriver">The Web Driver.</param>
        /// <returns>Web element or null.</returns>
        private IList<IWebElement> FindElementsIn(IWebDriver webDriver)
        {
            return webDriver.FindElements(
                this.SearchBy(this.FullSearchExpression));
        }

        /// <summary>
        /// Find element in another element.
        /// </summary>
        /// <param name="webElement">The Web Element</param>
        /// <returns>Web element or null.</returns>
        private IList<IWebElement> FindElementsIn(IWebElement webElement)
        {
            return webElement.FindElements(
                this.SearchBy(this.FullSearchExpressionForNestedComponent));
        }

        /// <summary>
        /// Find element in search context.
        /// </summary>
        /// <param name="searchContext">The search context.</param>
        /// <param name="by">Searching method.</param>
        /// <returns>Web element or null.</returns>
        private IWebElement FindElementIn(ISearchContext searchContext, By by)
        {
            try
            {
                return searchContext.FindElement(by);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}