﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents
{
    using System.Collections.Generic;
    using WebAutomation.Core.WebObjects.WebComponents.Actions;
    using WebAutomation.Core.WebObjects.WebComponents.States;
    using WebAutomation.Core.WebObjects.WebComponents.Value;
    using WebAutomation.Core.WebObjects.WebComponents.WebElementProviders;

    /// <summary>
    /// Represents an object on the page on which you can perform various operations and assertion.
    /// </summary>
    public interface IWebComponent
    {
        /// <summary>
        /// Gets the properties of Web Component.
        /// You can find here all attributes defined in XML for the current Web Component.
        /// </summary>
        Dictionary<string, string> Properties { get; }

        /// <summary>
        /// Assert that the current Web Component is in correct state or has a correct value.
        /// </summary>
        IAssert Assert { get; }

        /// <summary>
        /// Check whether the current Web Component is in a correct state (for example is displayed on the page).
        /// </summary>
        ICheckState Is { get; }

        /// <summary>
        /// Check (few times) whether the current Web Component will be in a correct state (for example will be displayed on the page).
        /// You can define maximum number of attempts of determining the state in the test settings.
        /// </summary>
        ICheckState WillBe { get; }

        /// <summary>
        /// Check whether the current Web Component has a correct value (for example text, CSS).
        /// </summary>
        ICheckValue Has { get; }

        /// <summary>
        /// Check (few times) whether the current Web Component will have a correct value (for example text, CSS).
        /// You can define maximum number of attempts of determining the value in the test settings.
        /// </summary>
        ICheckValue WillHave { get; }

        /// <summary>
        /// Perform an action (for example click, send text) on the current Web Component.
        /// </summary>
        IWebComponentActions Perform { get; }

        /// <summary>
        /// Perform an action (for example click, send text) only if the current Web Component exists on the page. 
        /// If Web Component is not present, the action will not be performed and test will be continued without error.
        /// </summary>
        IWebComponentActions PerformIfExists { get; }

        /// <summary>
        /// Gets or sets the Web Element Provider
        /// </summary>
        IWebElementProvider WebElementProvider { get; set; }

        /// <summary>
        /// Pass parameters to the current Web Component.
        /// These parameters will be used to find Web Components when searching by 'PXPATH', 'PID', etc.
        /// </summary>
        /// <param name="parameters">Parameters for Web Component.</param>
        /// <returns>The web Component with parameters.</returns>
        IWebComponent With(params string[] parameters);

        /// <summary>
        /// Search for Web Component inside another (parent) Web Component.
        /// </summary>
        /// <param name="parentComponent">The parent Web Component.</param>
        /// <returns>The Web Component.</returns>
        IWebComponent In(IWebComponent parentComponent);
    }
}
