﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents.Actions
{
    using WebAutomation.Core.WebObjects.Common;

    /// <summary>
    /// Change the checkbox selection.
    /// </summary>
    public class CheckAction : Action, ICheckAction
    {
        /// <summary>
        /// The settings.
        /// </summary>
        private ISettings settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckAction" /> class.
        /// </summary>      
        /// <param name="settings">The settings.</param>
        public CheckAction(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Check the checkbox.
        /// If the checkbox is already checked, no action will be performed.
        /// </summary>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public bool Check()
        {
            if (this.WebElement == null)
            {
                return false;
            }
        
            if (!this.WebElement.Selected)
            {
                this.WebElement.ScrollToMargin(this.WebDriver, this.settings.TopScrollMargin, this.settings.BottomScrollMargin);
                this.WebElement.Click();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Uncheck the checkbox.
        /// If the checkbox is already unchecked, no action will be performed.
        /// </summary>
        /// <returns>Value indicating whether the action has been performed.</returns>
        public bool Uncheck()
        {
            if (this.WebElement == null)
            {
                return false;
            }

            if (this.WebElement.Selected)
            {
                this.WebElement.ScrollToMargin(this.WebDriver, this.settings.TopScrollMargin, this.settings.BottomScrollMargin);
                this.WebElement.Click();
                return true;
            }

            return false;
        }
    }
}
