﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.WebComponents
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.Practices.Unity;
    using WebAutomation.Core.Logger;
    using WebAutomation.Core.WebObjects.WebComponents.Actions;
    using WebAutomation.Core.WebObjects.WebComponents.States;
    using WebAutomation.Core.WebObjects.WebComponents.Value;
    using WebAutomation.Core.WebObjects.WebComponents.WebElementProviders;

    /// <summary>
    /// Represents an object on the page on which you can perform various operations and assertion.
    /// </summary>
    public class WebComponent : IWebComponent
    {
        #region Private fields

        /// <summary>
        /// The properties.
        /// </summary>
        private Dictionary<string, string> properties;

        /// <summary>
        /// The assert object.
        /// </summary>
        private IWebComponentAssert assert;

        /// <summary>
        /// Current state of Web Component.
        /// </summary>
        private IWebComponentCheckState stateIs;

        /// <summary>
        /// Future state of Web Component.
        /// </summary>
        private IWebComponentCheckState stateWillBe;

        /// <summary>
        /// current value of Web Component.
        /// </summary>
        private IWebComponentCheckValue valueHas;

        /// <summary>
        /// Future value of Web Component.
        /// </summary>
        private IWebComponentCheckValue valueWillHave;

        /// <summary>
        /// Action object for Web Component.
        /// </summary>
        private IWebComponentActions actionPerform;

        /// <summary>
        /// Optional action object for Web Component.
        /// </summary>
        private IWebComponentActions actionPerformIfExists;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="WebComponent" /> class.
        /// </summary>
        /// <param name="assert">The assert object.</param>
        /// <param name="stateIs">Current state of Web Component.</param>
        /// <param name="stateWillBe">Future state of Web Component.</param>
        /// <param name="valueHas">current value of Web Component.</param>
        /// <param name="valueWillHave">Future value of Web Component.</param>
        /// <param name="actionPerform">Action object for Web Component.</param>
        /// <param name="actionPerformIfExists">Optional action object for Web Component.</param>
        /// <param name="logger">The logger.</param>
        public WebComponent(
            IWebComponentAssert assert,
            [Dependency("stateIs")] IWebComponentCheckState stateIs,
            [Dependency("stateWillBe")] IWebComponentCheckState stateWillBe,
            [Dependency("valueHas")] IWebComponentCheckValue valueHas,
            [Dependency("valueWillHave")] IWebComponentCheckValue valueWillHave,
            [Dependency("actionPerform")] IWebComponentActions actionPerform,
            [Dependency("actionPerformIfExists")] IWebComponentActions actionPerformIfExists,
            ILogger logger)
        {
            this.Logger = logger;
            this.assert = assert;
            this.stateIs = stateIs;
            this.stateWillBe = stateWillBe;
            this.valueHas = valueHas;
            this.valueWillHave = valueWillHave;
            this.actionPerform = actionPerform;
            this.actionPerformIfExists = actionPerformIfExists;

            this.properties = new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets the properties of Web Component.
        /// You can find here all attributes defined in XML for the current Web Component.
        /// </summary>
        public Dictionary<string, string> Properties
        {
            get
            {
                return this.properties;
            }
        }

        /// <summary>
        /// Assert that the current Web Component is in correct state or has a correct value.
        /// </summary>
        public IAssert Assert
        {
            get
            {
                this.assert.SetWebComponent(this);
                return this.assert;
            }
        }

        /// <summary>
        /// Check whether the current Web Component is in a correct state (for example is displayed on the page).
        /// </summary>
        public ICheckState Is
        {
            get
            {
                this.stateIs.WebElementProvider = this.WebElementProvider;
                return this.stateIs;
            }
        }

        /// <summary>
        /// Check (few times) whether the current Web Component will be in a correct state (for example will be displayed on the page).
        /// You can define maximum number of attempts of determining the state in the test settings.
        /// </summary>
        public ICheckState WillBe
        {
            get
            {
                this.stateWillBe.WebElementProvider = this.WebElementProvider;
                return this.stateWillBe;
            }
        }

        /// <summary>
        /// Check whether the current Web Component has a correct value (for example text, CSS).
        /// </summary>
        public ICheckValue Has
        {
            get
            {
                this.valueHas.WebElementProvider = this.WebElementProvider;
                return this.valueHas;
            }
        }

        /// <summary>
        /// Check (few times) whether the current Web Component will have a correct value (for example text, CSS).
        /// You can define maximum number of attempts of determining the value in the test settings.
        /// </summary>
        public ICheckValue WillHave
        {
            get
            {
                this.valueWillHave.WebElementProvider = this.WebElementProvider;
                return this.valueWillHave;
            }
        }

        /// <summary>
        /// Perform an action (for example click, send text) on the current Web Component.
        /// </summary>
        public IWebComponentActions Perform
        {
            get
            {
                this.actionPerform.WebElementProvider = this.WebElementProvider;
                return this.actionPerform;
            }
        }

        /// <summary>
        /// Perform an action (for example click, send text) only if the current Web Component exists on the page. 
        /// If Web Component is not present, the action will not be performed and test will be continued without error.
        /// </summary>
        public IWebComponentActions PerformIfExists
        {
            get
            {
                this.actionPerformIfExists.WebElementProvider = this.WebElementProvider;
                return this.actionPerformIfExists;
            }
        }

        /// <summary>
        /// Gets or sets the Web Element Provider
        /// </summary>
        public IWebElementProvider WebElementProvider { get; set; }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        protected ILogger Logger { get; private set; }

        /// <summary>
        /// Pass parameters to the current Web Component.
        /// These parameters will be used to find Web Components when searching by 'PXPATH', 'PID', etc.
        /// </summary>
        /// <param name="parameters">Parameters for Web Component.</param>
        /// <returns>The web Component with parameters.</returns>
        public virtual IWebComponent With(params string[] parameters)
        {
            var webComponent = this.Clone();
            webComponent.WebElementProvider.SearchParameters = parameters;
            return webComponent;
        }

        /// <summary>
        /// Search for Web Component inside another (parent) Web Component.
        /// </summary>
        /// <param name="parentComponent">The parent Web Component.</param>
        /// <returns>The Web Component.</returns>
        public IWebComponent In(IWebComponent parentComponent)
        {
            var webComponent = this.Clone();
            var webElementProvider = webComponent.WebElementProvider;
            while (webElementProvider.ParentWebElementProvider != null)
            {
                webElementProvider = webElementProvider.ParentWebElementProvider;
            }

            webElementProvider.ParentWebElementProvider = parentComponent.WebElementProvider.Clone();
            return webComponent;
        }

        /// <summary>
        /// Clone the current instance of Web Component.
        /// </summary>
        /// <returns>Cloned Web Component.</returns>
        private IWebComponent Clone()
        {
            var webComponent = new WebComponent(
                this.assert,
                this.stateIs,
                this.stateWillBe,
                this.valueHas,
                this.valueWillHave,
                this.actionPerform,
                this.actionPerformIfExists,
                this.Logger);

            webComponent.WebElementProvider = this.WebElementProvider.Clone();
            webComponent.properties = this.properties.ToDictionary(i => i.Key, i => i.Value);

            return webComponent;
        }
    }
}
