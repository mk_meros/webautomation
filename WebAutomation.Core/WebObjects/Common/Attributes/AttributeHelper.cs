﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.Common.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Attribute helper.
    /// </summary>
    internal static class AttributeHelper
    {
        /// <summary>
        /// Get a value of the attribute.
        /// </summary>
        /// <param name="attributeType">Attribute type.</param>
        /// <param name="webComponentPropertyInfo">Instance of the property info from which the value will be retrieved.</param>
        /// <returns>Attribute value.</returns>
        public static string GetAttributeValue(Type attributeType, PropertyInfo webComponentPropertyInfo)
        {
            if (attributeType == null)
            {
                return string.Empty;
            }

            CustomAttributeData attributeData = webComponentPropertyInfo
                   .CustomAttributes
                   .FirstOrDefault(a => a.AttributeType.Equals(attributeType));

            return GetValue(attributeData);
        }

        /// <summary>
        /// Get a value of the attribute.
        /// </summary>
        /// <param name="attributeType">Attribute type.</param>
        /// <param name="webContainerType">Type of Web Container object.</param>
        /// <returns>Attribute value or null if there is no proper attribute defined.</returns>
        public static string GetAttributeValue(Type attributeType, Type webContainerType)
        {
            CustomAttributeData attributeData = webContainerType
                   .CustomAttributes
                   .FirstOrDefault(a => a.AttributeType.Equals(attributeType));

            return AttributeHelper.GetValue(attributeData);
        }

        /// <summary>
        /// Get all custom attributes.
        /// </summary>
        /// <param name="webComponentPropertyInfo">Web Component property info.</param>
        /// <returns>The attributes.</returns>
        public static Dictionary<string, string> GetAttributes(PropertyInfo webComponentPropertyInfo)
        {
            var attributes = new Dictionary<string, string>();

            var customAttributes = webComponentPropertyInfo.CustomAttributes;
            foreach (var attr in customAttributes)
            {
                attributes.Add(attr.AttributeType.Name, AttributeHelper.GetValue(attr));
            }

            return attributes;
        }

        /// <summary>
        /// Get value of custom attribute.
        /// </summary>
        /// <param name="customAttributeData">Custom attribute data.</param>
        /// <returns>Attribute value.</returns>
        private static string GetValue(CustomAttributeData customAttributeData)
        {
            if (customAttributeData != null && customAttributeData.ConstructorArguments.Count > 0)
            {
                return (string)customAttributeData.ConstructorArguments[0].Value;
            }

            return string.Empty;
        }
    }
}
