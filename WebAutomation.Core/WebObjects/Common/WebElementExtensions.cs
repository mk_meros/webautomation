﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.WebObjects.Common
{
    using System;
    using System.Collections.Generic;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Interactions;

    /// <summary>
    /// Web element extensions.
    /// </summary>
    public static class WebElementExtensions
    {
        /// <summary>
        /// Get the 'value' attribute in current web element.
        /// </summary>
        /// <param name="webElement">The web element.</param>
        /// <returns>The value.</returns>
        public static string GetValue(this IWebElement webElement)
        {
            return webElement.GetAttribute("value");
        }

        /// <summary>
        /// Hover over the specified element.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="driver">The web driver.</param>
        /// <returns>The web element.</returns>
        public static IWebElement MouseHover(this IWebElement element, IWebDriver driver)
        {
            Actions builder = new Actions(driver);
            builder.MoveToElement(element);
            builder.Build().Perform();
            return element;
        }

        /// <summary>
        /// Scroll the web page to make the element to be visible.
        /// </summary>
        /// <param name="webElement">Web element.</param>
        /// <returns>The web element.</returns>
        [Obsolete("Scroll method is deprecated, please use ScrollToMargin instead.")]
        public static IWebElement Scroll(this IWebElement webElement)
        {
            try
            {
                var workaround = ((ILocatable)webElement).LocationOnScreenOnceScrolledIntoView;
            }
            catch (Exception)
            {
            }

            return webElement;
        }

        /// <summary>
        /// Scroll element to be visible (if necessary) according to given margin values.
        /// </summary>
        /// <param name="webElement">The Web Element.</param>
        /// <param name="webDriver">The Web Driver.</param>
        /// <param name="topMargin">Indicates the distance between element and the top edge of the browser after scrolling.</param>
        /// <param name="bottomMargin">Indicates the distance between element and the bottom edge of the browser after scrolling.</param>
        /// <returns>The Web  Element.</returns>
        public static IWebElement ScrollToMargin(this IWebElement webElement, IWebDriver webDriver, int topMargin = 100, int bottomMargin = 100)
        {
            const string GetWindowInfoScript = "return { 'scroll' : window.pageYOffset, 'height' : window.innerHeight };";
            
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            var data = (Dictionary<string, object>)js.ExecuteScript(GetWindowInfoScript);
            var scrollPosition = double.Parse(data["scroll"].ToString());
            var browserInnerHeight = double.Parse(data["height"].ToString());

            // Verify whether the element is above the browser window (including given top margin)           
            var webElementPositionTop = webElement.Location.Y;
            var topMinPosition = scrollPosition + topMargin;

            if (webElementPositionTop <= topMinPosition)
            {
                var offsetY = topMinPosition - webElementPositionTop;
                js.ExecuteScript(string.Format("window.scrollBy(0,{0})", -(int)offsetY));
                return webElement;
            }

            // Verify whether the element is below the browser window (including given bottom margin)
            var webElementPositionBottom = webElementPositionTop + webElement.Size.Height;
            var bottomMaxPosition = scrollPosition + browserInnerHeight - bottomMargin;

            if (webElementPositionBottom >= bottomMaxPosition)
            {
                var offsetY = webElementPositionBottom - bottomMaxPosition;
                js.ExecuteScript(string.Format("window.scrollBy(0,{0})", (int)offsetY));
                return webElement;
            }

            // Scrolling not required (web element should be visible)
            return webElement;
        }

        /// <summary>
        /// Check if given Web Element is displayed (is visible).
        /// </summary>
        /// <param name="webElement">Web Element.</param>
        /// <returns>Is element displayed.</returns>
        public static bool IsDisplayed(this IWebElement webElement)
        {
            if (webElement == null)
            {
                return false;
            }

            try
            {
                var displayValue = webElement.GetCssValue("display");
                return !(displayValue.Contains("none") || webElement.Size.IsEmpty);
            }
            catch (Exception)
            {
                // Element is not displayed
                return false;
            }
        }

        /// <summary>
        /// Select option by text.
        /// </summary>
        /// <param name="webElement">The Web Element.</param>
        /// <param name="text">The text.</param>
        /// <returns>Value indicating whether the option has been selected or not.</returns>
        public static bool SelectOption(this IWebElement webElement, string text)
        {
            foreach (IWebElement option in webElement.GetOptions())
            {
                if (option.Text.Trim() == text)
                {
                    if (!option.Selected)
                    {
                        option.Click();
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Select option by index.
        /// </summary>
        /// <param name="webElement">The Web Element.</param>
        /// <param name="index">The index.</param>
        /// <returns>Value indicating whether the option has been selected or not.</returns>
        public static bool SelectOption(this IWebElement webElement, int index)
        {
            var options = webElement.GetOptions();
            if (index < options.Count)
            {
                if (!options[index].Selected)
                {
                    options[index].Click();
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the selected option.
        /// </summary>
        /// <param name="webElement">The Web Element.</param>
        /// <returns>Selected option or null if not exists or not found.</returns>
        public static IWebElement SelectedOption(this IWebElement webElement)
        {
            foreach (IWebElement option in webElement.GetOptions())
            {
                if (option.Selected)
                {
                    return option;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the options.
        /// </summary>
        /// <param name="webElement">The Web Element.</param>
        /// <returns>All options.</returns>
        public static IList<IWebElement> GetOptions(this IWebElement webElement)
        {
            return webElement.FindElements(By.TagName("option"));
        }

        /// <summary>
        /// Gets a value indicating whether the Web Element represents the IFrame.
        /// </summary>
        /// <param name="webElement">The Web Element.</param>
        /// <returns>Is IFrame.</returns>
        public static bool IsIFrame(this IWebElement webElement)
        {
            return webElement.TagName.ToLower().Equals("iframe");
        }
    }
}
