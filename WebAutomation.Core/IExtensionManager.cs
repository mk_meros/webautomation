﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using WebAutomation.Core.WebObjects.Common.Activities;
    using WebAutomation.Core.WebObjects.WebComponents;
    using WebAutomation.Core.WebObjects.WebComponents.Actions;

    /// <summary>
    /// Extension Manager is used to replace default implementation of framework with your own custom extensions.
    /// </summary>
    public interface IExtensionsManager
    {
        /// <summary>
        /// Register activity with the given type.
        /// </summary>
        /// <param name="name">The name of activity (is only required if you want to register the same type multiple times).</param>
        /// <typeparam name="TActivity">Type of activity.</typeparam>
        void RegisterActivity<TActivity>(string name = null) where TActivity : IActivity;

        /// <summary>
        /// Define the custom type for WebComponent.
        /// </summary>
        /// <typeparam name="TWebComponent">Type of WebComponent.</typeparam>
        void DefineWebComponent<TWebComponent>() where TWebComponent : IWebComponent;

        /// <summary>
        /// Define the custom type for CheckAction.
        /// </summary>
        /// <typeparam name="TCheckAction">Type of CheckAction.</typeparam>
        void DefineCheckAction<TCheckAction>() where TCheckAction : ICheckAction;

        /// <summary>
        /// Define the custom type for ClearAction.
        /// </summary>
        /// <typeparam name="TClearAction">Type of ClearAction.</typeparam>
        void DefineClearAction<TClearAction>() where TClearAction : IClearAction;

        /// <summary>
        /// Define the custom type for ClickAction.
        /// </summary>
        /// <typeparam name="TClickAction">Type of ClearAction</typeparam>
        void DefineClickAction<TClickAction>() where TClickAction : IClickAction;

        /// <summary>
        /// Define the custom type for DragAndDropAction.
        /// </summary>
        /// <typeparam name="TDragAndDropAction">Type of DragAndDropAction.</typeparam>
        void DefineDragAndDropAction<TDragAndDropAction>() where TDragAndDropAction : IDragAndDropAction;

        /// <summary>
        /// Define the custom type for FillAction.
        /// </summary>
        /// <typeparam name="TFillAction">Type of FillAction.</typeparam>
        void DefineFillAction<TFillAction>() where TFillAction : IFillAction;

        /// <summary>
        /// Define the custom type for HoverAction.
        /// </summary>
        /// <typeparam name="THoverAction">Type of HoverAction.</typeparam>
        void DefineHoverAction<THoverAction>() where THoverAction : IHoverAction;
        
        /// <summary>
        /// Define the custom type for SelectAction.
        /// </summary>
        /// <typeparam name="TSelectAction">Type of SelectAction.</typeparam>
        void DefineSelectAction<TSelectAction>() where TSelectAction : ISelectAction;

        /// <summary>
        /// Define the custom type for ScrollAction.
        /// </summary>
        /// <typeparam name="TScrollAction">Type of ScrollAction.</typeparam>
        void DefineScrollAction<TScrollAction>() where TScrollAction : IScrollAction;
    }
}
