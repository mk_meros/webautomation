﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core.Logger
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Defines the formatter for log messages.
    /// </summary>
    public static class LogFormatter
    {
        /// <summary>
        /// Get formatted log message.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns>Formatted log message.</returns>
        public static string GetLog(Dictionary<string, object> parameters)
        {
            StringBuilder log = new StringBuilder();
            int nameLength = parameters
                                .Select(p => p.Key.Trim())
                                .OrderByDescending(p => p.Length)
                                .ElementAt(0)
                                .Length;

            foreach (var nameValue in parameters)
            {
                string name = string.IsNullOrEmpty(nameValue.Key)
                              ? string.Empty
                              : nameValue.Key.Trim();

                string prefix = string.Format(
                              "\t> {0}{1} : ",
                              name,
                              new string(' ', nameLength - name.Length));

                string value = nameValue.Value == null
                              ? string.Empty
                              : nameValue.Value.ToString();

                value = string.IsNullOrEmpty(value)
                              ? string.Empty
                              : value
                                   .Trim()
                                   .Replace("\n", "\n\t" + new string(' ', prefix.Length - 1));
                
                log.AppendFormat("{0}{1}\n", prefix, value);
            }

            return log.ToString();
        }

        /// <summary>
        /// Convert given log to second level log.
        /// </summary>
        /// <param name="log">The log.</param>
        /// <returns>Second level log.</returns>
        public static string GetSecondLevelLog(string log)
        {
            return "\t" + log.Replace("\n", "\n\t");
        }
    }
}
