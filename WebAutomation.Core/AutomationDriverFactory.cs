﻿namespace WebAutomation.Core
{
    /// <summary>
    /// AutomationDriver factory.
    /// </summary>
    public class AutomationDriverFactory
    {
        /// <summary>
        /// Get new instance of AutomationDriver.
        /// </summary>
        /// <returns>The AutomationDriver.</returns>
        public static IAutomationDriver Get()
        {
            return new AutomationDriver();
        }
    }
}
