﻿namespace WebAutomation.Core
{
    using System;

    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Defines Automation Driver actions.
    /// </summary>
    public interface IAutomationDriver
    {
        /// <summary>
        /// Gets or sets the WebDriver for the current session (see Sessions.CurrentSession).
        /// </summary>
        IWebDriver WebDriver { get; set; }

        /// <summary>
        /// Gets or sets the Settings.
        /// </summary>
        ISettings Settings { get; set; }

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        ILogger Logger { get; set; }

        /// <summary>
        /// Gets the session manager.
        /// </summary>
        ISessionManager Sessions { get; }

        /// <summary>
        /// Gets the extensions manager.
        /// </summary>
        IExtensionsManager Extensions { get; }

        /// <summary>
        /// Get a Web Container.
        /// </summary>
        /// <typeparam name="TWebContainer">Type of Web Container.</typeparam>
        /// <returns>The Web Container.</returns>
        TWebContainer Get<TWebContainer>() where TWebContainer : new();

        /// <summary>
        /// Get a Web Container.
        /// </summary>
        /// <param name="webContainerType">Type of Web Container.</param>
        /// <returns>The Web Container.</returns>
        object Get(Type webContainerType);

        /// <summary>
        /// Quit browser.
        /// </summary>
        /// <param name="quitBrowsersInAllSessions">Quit browser in all sessions.</param>
        void QuitBrowser(bool quitBrowsersInAllSessions = false);
    }
}