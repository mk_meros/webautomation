﻿//-----------------------------------------------------------------------
// Copyright (c) 2016 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using Microsoft.Practices.Unity;
    using WebAutomation.Core.WebObjects.Common.Activities;
    using WebAutomation.Core.WebObjects.WebComponents;
    using WebAutomation.Core.WebObjects.WebComponents.Actions;

    /// <summary>
    /// Extension Manager is used to replace default implementation of framework with your own custom extensions.
    /// </summary>
    internal class ExtensionsManager : IExtensionsManager
    {
        /// <summary>
        /// Dependency container.
        /// </summary>
        private IUnityContainer container;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtensionsManager" /> class.
        /// </summary>
        /// <param name="container">Dependency container.</param>
        public ExtensionsManager(IUnityContainer container)
        {
            this.container = container;
        }

        /// <summary>
        /// Register activity with the given type.
        /// </summary>
        /// <param name="name">The name of activity (is only required if you want to register the same type multiple times).</param>
        /// <typeparam name="TActivity">Type of activity.</typeparam>
        public void RegisterActivity<TActivity>(string name = null) where TActivity : IActivity
        {
            if (string.IsNullOrEmpty(name))
            {
                name = typeof(TActivity).FullName;
            }

            this.container.RegisterType(typeof(IActivity), typeof(TActivity), name);
        }

        /// <summary>
        /// Define the custom type for WebComponent.
        /// </summary>
        /// <typeparam name="TWebComponent">Type of WebComponent.</typeparam>
        public void DefineWebComponent<TWebComponent>() where TWebComponent : IWebComponent
        {
            this.container.RegisterType<IWebComponent, TWebComponent>();
        }

        /// <summary>
        /// Define the custom type for CheckAction.
        /// </summary>
        /// <typeparam name="TCheckAction">Type of CheckAction.</typeparam>
        public void DefineCheckAction<TCheckAction>() where TCheckAction : ICheckAction
        {
            this.container.RegisterType<ICheckAction, TCheckAction>();
        }

        /// <summary>
        /// Define the custom type for ClearAction.
        /// </summary>
        /// <typeparam name="TClearAction">Type of ClearAction.</typeparam>
        public void DefineClearAction<TClearAction>() where TClearAction : IClearAction
        {
            this.container.RegisterType<IClearAction, TClearAction>();
        }

        /// <summary>
        /// Define the custom type for ClickAction.
        /// </summary>
        /// <typeparam name="TClickAction">Type of ClearAction</typeparam>
        public void DefineClickAction<TClickAction>() where TClickAction : IClickAction
        {
            this.container.RegisterType<IClickAction, TClickAction>();
        }

        /// <summary>
        /// Define the custom type for DragAndDropAction.
        /// </summary>
        /// <typeparam name="TDragAndDropAction">Type of DragAndDropAction.</typeparam>
        public void DefineDragAndDropAction<TDragAndDropAction>() where TDragAndDropAction : IDragAndDropAction
        {
            this.container.RegisterType<IDragAndDropAction, TDragAndDropAction>();
        }

        /// <summary>
        /// Define the custom type for FillAction.
        /// </summary>
        /// <typeparam name="TFillAction">Type of FillAction.</typeparam>
        public void DefineFillAction<TFillAction>() where TFillAction : IFillAction
        {
            this.container.RegisterType<IFillAction, TFillAction>();
        }

        /// <summary>
        /// Define the custom type for HoverAction.
        /// </summary>
        /// <typeparam name="THoverAction">Type of HoverAction.</typeparam>
        public void DefineHoverAction<THoverAction>() where THoverAction : IHoverAction
        {
            this.container.RegisterType<IHoverAction, THoverAction>();
        }

        /// <summary>
        /// Define the custom type for SelectAction.
        /// </summary>
        /// <typeparam name="TSelectAction">Type of SelectAction.</typeparam>
        public void DefineSelectAction<TSelectAction>() where TSelectAction : ISelectAction
        {
            this.container.RegisterType<ISelectAction, TSelectAction>();
        }

        /// <summary>
        /// Define the custom type for ScrollAction.
        /// </summary>
        /// <typeparam name="TScrollAction">Type of ScrollAction.</typeparam>
        public void DefineScrollAction<TScrollAction>() where TScrollAction : IScrollAction
        {
            this.container.RegisterType<IScrollAction, TScrollAction>();
        }
    }
}
