﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------
namespace WebAutomation.Core
{
    using System;
    using System.IO;
    using System.Reflection;
   
    using OpenQA.Selenium;
    using WebAutomation.Core.Logger;

    /// <summary>
    /// Base class for all tests.
    /// </summary>
    [Obsolete("Class no longer supported. Instead, please create a new instance of AutomationDriver.", false)]

    public abstract class TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase" /> class.
        /// </summary>
        /// <param name="createAutomationDriver">Set to 'false' only if you want to initialize later manually.</param>
        public TestBase(bool createAutomationDriver = true)
        {
            if (createAutomationDriver)
            {
                this.AutomationDriver = AutomationDriverFactory.Get();
                this.CurrentWebContainer = new object();
            }
        }

        /// <summary>
        /// Gets the assembly directory.
        /// </summary>
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        /// <summary>
        /// Gets or sets the Automation Driver.
        /// </summary>
        public virtual IAutomationDriver AutomationDriver { get; set; }

        /// <summary>
        /// Gets or sets the Web Driver for the current session (see SessionManager).
        /// During setting, the ImplicitlyWait will be set to default value and current container will be removed.
        /// </summary>
        public IWebDriver WebDriver
        {
            get
            {
                return this.AutomationDriver.WebDriver;
            }

            set
            {
                this.AutomationDriver.WebDriver = value;
                this.CurrentWebContainer = new object();
            }
        }

        /// <summary>
        /// Gets the session manager.
        /// </summary>
        public ISessionManager SessionManager
        {
            get
            {
                return this.AutomationDriver.Sessions;
            }
        }

        /// <summary>
        /// Gets the extensions manager.
        /// </summary>
        public IExtensionsManager Extensions
        {
            get
            {
                return this.AutomationDriver.Extensions;
            }
        }

        /// <summary>
        /// Gets the Settings.
        /// </summary>
        public ISettings Settings
        {
            get
            {
                return this.AutomationDriver.Settings;
            }
        }

        /// <summary>
        /// Gets the logger.
        /// </summary>
        public ILogger Logger
        {
            get
            {
                return this.AutomationDriver.Logger;
            }
        }

        /// <summary>
        /// Gets or sets the Current Web Container.
        /// </summary>
        public virtual object CurrentWebContainer { get; protected set; }

        /// <summary>
        /// Get the Web Container of given type.
        /// If the specified type is different from the present, the Web Container will first be loaded.
        /// </summary>
        /// <typeparam name="TWebContainer">Type of Web Container.</typeparam>
        /// <param name="forceReload">Indicates whether the Web Container should be reloaded (in case if it is already loaded).</param>
        /// <returns>The Web Container.</returns>
        public TWebContainer GetContainer<TWebContainer>(bool forceReload = false) where TWebContainer : new()
        {
            return (TWebContainer)this.GetContainer(typeof(TWebContainer), forceReload);
        }

        /// <summary>
        /// Get the Web Container of given type.
        /// If the specified type is different from the present, the Web Container will first be loaded.
        /// </summary>
        /// <param name="webContainerType">Type of Web Container.</param>
        /// <param name="forceReload">Indicates whether the Web Container should be reloaded (in case if it is already loaded).</param>
        /// <returns>The Web Container.</returns>
        public object GetContainer(Type webContainerType, bool forceReload = false)
        {
            if (forceReload || !this.CurrentWebContainer.GetType().Equals(webContainerType))
            {
                this.CurrentWebContainer = this.AutomationDriver.Get(webContainerType);
            }

            return this.CurrentWebContainer;
        }

        /// <summary>
        /// Quit browser in the current session.
        /// </summary>
        public virtual void QuitBrowser()
        {
            this.AutomationDriver.QuitBrowser();
        }

        /// <summary>
        /// Quit browsers from all sessions.
        /// </summary>
        public void QuitBrowsersFromAllSessions()
        {
            this.AutomationDriver.QuitBrowser(true);
        }
    }
}