﻿Feature: ValueTests

Scenario: SpecFlow - value tests
	Given user navigates to test page

	Then the 'SampleWebSite-TextSpan' has text equal to 'Sample text in span'
	Then the 'SampleWebSite-TextSpan' has text not equal to 'AA text in span'
	Then the 'SampleWebSite-TextSpan' has text which contains 'text'
	Then the 'SampleWebSite-TextSpan' has text which not contains 'ina'
	Then the 'SampleWebSite-TextSpan' has text which starts with 'Sample'
	Then the 'SampleWebSite-TextSpan' has text which ends with 'span'

	Then the 'SampleWebSite-TextSpan' will have text equal to 'Sample text in span'
	Then the 'SampleWebSite-TextSpan' will have text not equal to 'AA text in span'
	Then the 'SampleWebSite-TextSpan' will have text which contains 'text'
	Then the 'SampleWebSite-TextSpan' will have text which not contains 'ina'
	Then the 'SampleWebSite-TextSpan' will have text which starts with 'Sample'
	Then the 'SampleWebSite-TextSpan' will have text which ends with 'span'

	Then the 'SampleWebSite-Textbox' has value equal to 'Sample text in textbox'
	Then the 'SampleWebSite-Textbox' will have value equal to 'Sample text in textbox'

	Then the 'SampleWebSite-TextSpan' has text length equal to '19'
	Then the 'SampleWebSite-TextSpan' has text length not equal to '20'
	Then the 'SampleWebSite-TextSpan' has text length greater than '18'
	Then the 'SampleWebSite-TextSpan' has text length greater than or equal to '19'
	Then the 'SampleWebSite-TextSpan' has text length less than '20'
	Then the 'SampleWebSite-TextSpan' has text length less than or equal to '19'
	Then the 'SampleWebSite-TextSpan' will have text length equal to '19'

	Then the 'SampleWebSite-Textbox' has value length equal to '22'
	Then the 'SampleWebSite-Textbox' will have value length equal to '22'

	Then the 'SampleWebSite-Select' has option 'Option 1' selected
	Then the 'SampleWebSite-Select' has option 'Option 2' not selected
	Then the 'SampleWebSite-Select' has option 'Option 3' available
	Then the 'SampleWebSite-Select' has option 'Option 4' not available
	Then the 'SampleWebSite-Select' will have option 'Option 1' selected

	Then the 'SampleWebSite-DivWithBackground' has CSS attribute 'background-color' with value which contains '255'
	Then the 'SampleWebSite-DivWithBackground' will have CSS attribute 'background-color' with value which contains '255'

	Then the 'SampleWebSite-HiddenText' has attribute 'style' with value which contains 'display'
	Then the 'SampleWebSite-HiddenText' will have attribute 'style' with value which contains 'display'

	Then the 'SampleWebSite-Options1' has elements count equal to '3'
	Then the 'SampleWebSite-Options1' will have elements count equal to '3'

	Then the 'SampleWebSite-Options1' has elements sorted alphabetically