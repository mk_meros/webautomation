﻿Feature: StateTests

Scenario: SpecFlow - state tests
	Given user navigates to test page

	Then the 'SampleWebSite-DivWithBackground' is present
	Then the 'SampleWebSite-DivWithBackground' will be present

	Then the 'SampleWebSite-DivWithBackground' is displayed
	Then the 'SampleWebSite-DivWithBackground' will be displayed

	Then the 'SampleWebSite-DivWithBackground' is present
	Then the 'SampleWebSite-DivWithBackground' will be present
						   
	Then the 'SampleWebSite-HiddenText' is not displayed
	Then the 'SampleWebSite-HiddenText' will be not displayed

	Then the 'SampleWebSite-EnabledButton' is enabled
	Then the 'SampleWebSite-EnabledButton' will be enabled

	Then the 'SampleWebSite-DisabledButton' is not enabled
	Then the 'SampleWebSite-DisabledButton' will be not enabled

	Then the 'SampleWebSite-CheckboxChecked' is checked
	Then the 'SampleWebSite-CheckboxChecked' will be checked

	Then the 'SampleWebSite-CheckboxNotChecked' is not checked
	Then the 'SampleWebSite-CheckboxNotChecked' will be not checked