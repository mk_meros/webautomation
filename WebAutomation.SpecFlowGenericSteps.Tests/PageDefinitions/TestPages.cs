﻿//-----------------------------------------------------------------------
// Copyright (c) 2015 Marek Kudliński (meros)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//-----------------------------------------------------------------------

#region Designer generated code
#pragma warning disable

namespace WebAutomation.SpecFlowGenericSteps.Tests
{
	using WebAutomation.Core.WebObjects.Common.Attributes;
	using WebAutomation.Core.WebObjects.WebComponents;
	using WebAutomation.Core.WebObjects.WebComponents.Attributes;
	using WebAutomation.Core.WebObjects.WebContainer;
	using WebAutomation.Core.WebObjects.WebContainer.Attributes;

	[Name("SampleWebSite")]
	public partial class SampleWebSite 
	{
		[Name("DivWithBackground")]
		[Xpath("//*[@id='DivWithBackground']")]
		public IWebComponent DivWithBackground { get; set; }

		[Name("TextSpan")]
		[Xpath("//*[@id='TextSpan']")]
		public IWebComponent TextSpan { get; set; }

		[Name("HiddenText")]
		[Xpath("//*[@id='HiddenText']")]
		public IWebComponent HiddenText { get; set; }

		[Name("EnabledButton")]
		[Xpath("//*[@id='EnabledButton']")]
		public IWebComponent EnabledButton { get; set; }

		[Name("DisabledButton")]
		[Xpath("//*[@id='DisabledButton']")]
		public IWebComponent DisabledButton { get; set; }

		[Name("Checkbox")]
		[Xpath("//*[@id='Checkbox']")]
		public IWebComponent Checkbox { get; set; }

		[Name("CheckboxNotChecked")]
		[Xpath("//*[@id='CheckboxNotChecked']")]
		public IWebComponent CheckboxNotChecked { get; set; }

		[Name("CheckboxChecked")]
		[Xpath("//*[@id='CheckboxChecked']")]
		public IWebComponent CheckboxChecked { get; set; }

		[Name("Textbox")]
		[Xpath("//*[@id='Textbox']")]
		public IWebComponent Textbox { get; set; }

		[Name("TextboxRandom")]
		[Xpath("//*[@id='TextboxRandom']")]
		public IWebComponent TextboxRandom { get; set; }

		[Name("Select")]
		[Xpath("//*[@id='Select']")]
		public IWebComponent Select { get; set; }

		[Name("SelectRandom")]
		[Xpath("//*[@id='SelectRandom']")]
		public IWebComponent SelectRandom { get; set; }

		[Name("Options1")]
		[Xpath("//option[starts-with(@id, 'Option1')]")]
		public IWebComponent Options1 { get; set; }

		[Name("Options2")]
		[Xpath("//option[starts-with(@id, 'Option2')]")]
		public IWebComponent Options2 { get; set; }

		[Name("ElementNull")]
		[Id("FakeElemetn")]
		public IWebComponent ElementNull { get; set; }

		[Name("TableRow")]
		[Pxpath("//table/tbody/tr[./td[1][text()='{0}']][./td[2][text()='{1}']][./td[3][text()='{2}']][./td[4][text()='{3}']]")]
		public IWebComponent TableRow { get; set; }

	}

}

#pragma warning restore
#endregion

